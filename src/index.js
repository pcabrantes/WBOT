const puppeteer = require('puppeteer-core');
const _cliProgress = require('cli-progress');
const spintax = require('mel-spintax');
require("./welcome");
var spinner = require("./step");
var utils = require("./utils");
var qrcode = require('qrcode-terminal');
var path = require("path");
var argv = require('yargs').argv;
var rev = require("./detectRev");
var constants = require("./constants");
var configs = require("../bot");
var settings = require('./settings');
var fs = require("fs");
var AWS = require('aws-sdk');
const { Consumer } = require('sqs-consumer');
const queueURL = "https://sqs.us-east-1.amazonaws.com/874138717341/wbot-homolog";

//console.log(ps);

//console.log(process.cwd());

async function Main() {

    try {
        //console.log(configs);
        var page;
        await downloadAndStartThings();
        var isLogin = await checkLogin();
        if (!isLogin) {
            await getAndShowQR();
        }
        if (configs.smartreply.suggestions.length > 0) {
            await setupSmartReply();
        }

        await listenSQS();

        console.log("WBOT is ready !! Let those message come.");
    } catch (e) {
        console.error("\nLooks like you got an error. " + e);
        try {
            page.screenshot({ path: path.join(process.cwd(), "error.png") })
        } catch (s) {
            console.error("Can't create shreenshot, X11 not running?. " + s);
        }
        console.warn(e);
        console.error("Don't worry errors are good. They help us improve. A screenshot has already been saved as error.png in current directory. Please mail it on vasani.arpit@gmail.com along with the steps to reproduce it.\n");
        throw e;
    }

    /**
     * If local chrome is not there then this function will download it first. then use it for automation. 
     */
    async function downloadAndStartThings() {
        let botjson = utils.externalInjection("bot.json");
        var appconfig = await utils.externalInjection("bot.json");
        appconfig = JSON.parse(appconfig);
        spinner.start("Downloading chrome\n");
        const browserFetcher = puppeteer.createBrowserFetcher({
            path: process.cwd()
        });
        const progressBar = new _cliProgress.Bar({}, _cliProgress.Presets.shades_grey);
        progressBar.start(100, 0);
        var revNumber = await rev.getRevNumber();
        const revisionInfo = await browserFetcher.download(revNumber, (download, total) => {
            //console.log(download);
            var percentage = (download * 100) / total;
            progressBar.update(percentage);
        });
        progressBar.update(100);
        spinner.stop("Downloading chrome ... done!");
        //console.log(revisionInfo.executablePath);
        spinner.start("Launching Chrome");
        var pptrArgv = [];
        if (argv.proxyURI) {
            pptrArgv.push('--proxy-server=' + argv.proxyURI);
        }
        const extraArguments = Object.assign({});
        extraArguments.userDataDir = constants.DEFAULT_DATA_DIR;
        const browser = await puppeteer.launch({
            executablePath: revisionInfo.executablePath,
            defaultViewport: null,
            headless: appconfig.appconfig.headless,
            userDataDir: path.join(process.cwd(), "ChromeSession"),
            devtools: false,
            args: [...constants.DEFAULT_CHROMIUM_ARGS, ...pptrArgv], ...extraArguments
        });
        spinner.stop("Launching Chrome ... done!");
        if (argv.proxyURI) {
            spinner.info("Using a Proxy Server");
        }
        spinner.start("Opening Whatsapp");
        page = await browser.pages();
        if (page.length > 0) {
            page = page[0];
            page.setBypassCSP(true);
            if (argv.proxyURI) {
                await page.authenticate({ username: argv.username, password: argv.password });
            }
            page.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36");
            await page.goto('https://web.whatsapp.com', {
                waitUntil: 'networkidle0',
                timeout: 0
            });
            //console.log(contents);
            //await injectScripts(page);
            botjson.then((data) => {
                page.evaluate("var intents = " + data);
                //console.log(data);
            }).catch((err) => {
                console.log("there was an error \n" + err);
            });
            spinner.stop("Opening Whatsapp ... done!");
            page.exposeFunction("log", (message) => {
                console.log(message);
            });

            // When the settings file is edited multiple calls are sent to function. This will help
            // to prevent from getting corrupted settings data
            let timeout = 5000;
            
            // Register a filesystem watcher
            fs.watch(constants.BOT_SETTINGS_FILE, (event, filename) => {
                setTimeout(()=> {
                    settings.LoadBotSettings(event, filename, page);
                }, timeout);
            });

            page.exposeFunction("getFile", utils.getFileInBase64);
            page.exposeFunction("saveFile", utils.saveFileFromBase64);
            page.exposeFunction("resolveSpintax", spintax.unspin);
        }
    }

    async function injectScripts(page) {
        return await page.waitForSelector('[data-icon=laptop]')
            .then(async () => {
                var filepath = path.join(__dirname, "WAPI.js");
                await page.addScriptTag({ path: require.resolve(filepath) });
                filepath = path.join(__dirname, "inject.js");
                await page.addScriptTag({ path: require.resolve(filepath) });
                return true;
            })
            .catch(() => {
                console.log("User is not logged in. Waited 30 seconds.");
                return false;
            })
    }

    async function checkLogin() {
        spinner.start("Page is loading");
        //TODO: avoid using delay and make it in a way that it would react to the event. 
        await utils.delay(10000);
        //console.log("loaded");
        var output = await page.evaluate("localStorage['last-wid']");
        //console.log("\n" + output);
        if (output) {
            spinner.stop("Looks like you are already logged in");
            await injectScripts(page);
        } else {
            spinner.info("You are not logged in. Please scan the QR below");
        }
        return output;
    }

    //TODO: add logic to refresh QR.
    async function getAndShowQR() {
        //TODO: avoid using delay and make it in a way that it would react to the event. 
        //await utils.delay(10000);
        var scanme = "img[alt='Scan me!'], canvas";
        await page.waitForSelector(scanme);
        var imageData = await page.evaluate(`document.querySelector("${scanme}").parentElement.getAttribute("data-ref")`);
        //console.log(imageData);
        qrcode.generate(imageData, { small: true });
        spinner.start("Waiting for scan \nKeep in mind that it will expire after few seconds");
        var isLoggedIn = await injectScripts(page);
        while (!isLoggedIn) {
            //console.log("page is loading");
            //TODO: avoid using delay and make it in a way that it would react to the event. 
            await utils.delay(300);
            isLoggedIn = await injectScripts(page);
        }
        if (isLoggedIn) {
            spinner.stop("Looks like you are logged in now");
            //console.log("Welcome, WBOT is up and running");
        }
    }

    async function setupSmartReply() {
        spinner.start("setting up smart reply");
        await page.waitForSelector("#app");
        await page.evaluate(`
            var observer = new MutationObserver((mutations) => {
                for (var mutation of mutations) {
                    //console.log(mutation);
                    if (mutation.addedNodes.length && mutation.addedNodes[0].id === 'main') {
                        //newChat(mutation.addedNodes[0].querySelector('.copyable-text span').innerText);
                        console.log("%cChat changed !!", "font-size:x-large");
                        WAPI.addOptions();
                    }
                }
            });
            observer.observe(document.querySelector('#app'), { attributes: false, childList: true, subtree: true });
        `);
        spinner.stop("setting up smart reply ... done!");
        page.waitForSelector("#main", { timeout: 0 }).then(async () => {
            await page.exposeFunction("sendMessage", async message => {
                return new Promise(async (resolve, reject) => {
                    //send message to the currently open chat using power of puppeteer 
                    await page.type("#main div.selectable-text[data-tab]", message);
                    if (configs.smartreply.clicktosend) {
                        await page.click("#main > footer > div.copyable-area > div:nth-child(3) > button");
                    }
                });
            });
        });
    }

    async function listenSQS() {
        
        console.log("Iniciando comunicação com a fila");

        page.on('console', msg => {
            for (let i = 0; i < msg.args().length; ++i)
              console.log(`${i}: ${msg.args()[i]}`);
          });

        const app = Consumer.create({
            queueUrl: queueURL,
            handleMessage: async (message) => {
                console.log(message)
                let jsonMessage = JSON.parse(message.Body)
                // let destinatario = '5511996765526-1598296554@g.us'
                let destinatario = '5511955503933@c.us'

                if (jsonMessage.tipo != null && jsonMessage.tipo == "0") {
                    destinatario = `558388993933@c.us`
                } else if (jsonMessage.destinatario != null) {
                    //destinatario = `55${jsonMessage.destinatario}@c.us`
                    destinatario = `5511955503933@c.us`
                } else if (jsonMessage.tipo == 4) {
                    jsonMessage.destinatario = '5511951751108'
                    destinatario = '5511951751108@c.us'
                }

                console.log(`Destinatario: ${destinatario}`)

                await page.evaluate((dest, msg) => {
                    if (msg.fileName == null) {
                        console.log('Enviando mensagem de texto')
                        WAPI.sendMessage(dest, msg.mensagem)
                    } else { 
                        console.log('Enviando imagem')
                        if (msg.tipo == 4) {
                            console.log('Enviando imagem de pre-atendimento')
                            window.getFileFromS3(msg.fileName).then((base64Data) => {
                                console.log("Base64: " + base64Data)
                                WAPI.sendImage(base64Data, dest, msg.fileName);
                            }).catch((error) => {
                                window.log("Error in sending file\n" + error);
                            })
                        } else if (msg.tipo == 5) {
                            console.log('Enviando imagem de boas vindas')
                            window.getFile(`imagens/boas-vindas/${msg.fileName}`).then((base64Data) => {
                                WAPI.sendImage(base64Data, dest, msg.fileName, msg.mensagem);
                            }).catch((error) => {
                                window.log("Error in sending file\n" + error);
                            })
                        }
                    }
                }, destinatario, jsonMessage)
            }
        });

        app.on('error', (err, message) => {
            console.error(err.message);
            console.error('Mensagem com erro:');
            console.error(JSON.stringify(message.Body));
        });
           
        app.on('processing_error', (err, message) => {
            console.error(err.message);
            console.error('Mensagem com erro de processamento:');
            console.error(JSON.stringify(message.Body));
        });

        app.on('timeout_error', (err, message) => {
            console.error(err.message);
            console.error('Mensagem com erro de timeout:');
            console.error(JSON.stringify(message.Body));
        });

        app.on('message_received', (message) => {
            console.log('Mensagem recebida');
            console.log(JSON.stringify(message.Body))
        });

        app.on('message_processed', (message) => {
            console.log('Mensagem processada');
            
            page.evaluate((processed) => {

                let jsonMessage = JSON.parse(processed.Body)

                if (jsonMessage.tipo == 1) {
                    //let chat = WAPI.getChat('5511996765526-1598296554@g.us');
                    let chat = WAPI.getChat('5511996765526-1598296554@g.us');
                    let messages = chat.msgs._models;
                    let last = messages[messages.length-1].body                

                    if (last == jsonMessage.mensagem) {
                        console.log('Mensagem enviada com sucesso')
                    } else {
                        console.log('Mensagem não enviada')
                        console.log('Tentando novo envio')
                    
                        chat.sendMessage(jsonMessage.mensagem)

                        //let chat = WAPI.getChat('5511996765526-1598296554@g.us');
                        let chat = WAPI.getChat('5511996765526-1598296554@g.us');

                        messages = chat.msgs._models;
                        last = messages[messages.length-1].body

                        if (last != jsonMessage.mensagem) {
                            console.error('Mensagem não enviada')
                            console.error(jsonMessage.mensagem)
                        }
                    }
                }
                
            }, message)

            let jsonMessage = JSON.parse(message.Body)

            if (jsonMessage.tipo == 5 && jsonMessage.fileName != null) {
                console.log(`Enviada mensagem de boas vindas para ${jsonMessage.destinatario}`)
            }
        });
        app.start();
    }
}

Main();
